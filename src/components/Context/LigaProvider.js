import React, { Component } from 'react';
export const StoreContext = React.createContext();

class LigaProvider extends Component {

    state = {
        equipos: [
          {
            nombre:   'Real Madrid',
            titulos:  25
          },
          {
            nombre:   'Barcelona',
            titulos:  19
          },
          {
            nombre:   'Manchester United',
            titulos:  12
          }
        ],
        mensaje: 'Hola mundo'
      }

    render() {
        return (
            <StoreContext.Provider 
                value={{
                    equipos: this.state.equipos,
                    mensaje: this.state.mensaje,
                    sumarTitulo: (id) => {
                      const equipos = [...this.state.equipos];
                      equipos[id].titulos = equipos[id].titulos + 1;
                      this.setState( { equipos } );
                    },
                    restarTitulo: (id) => {
                      const equipos = [...this.state.equipos];
                      equipos[id].titulos = equipos[id].titulos - 1;
                      this.setState( { equipos } );
                    }
                }}>
              {this.props.children}
            </StoreContext.Provider>
        );
    }
}

export default LigaProvider;