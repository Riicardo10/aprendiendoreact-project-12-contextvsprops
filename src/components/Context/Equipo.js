import React, { Component } from 'react';
import {StoreContext} from './LigaProvider';

class Equipo extends Component {
    render() {
        return (
            <StoreContext.Consumer>
                { (value) => {
                    return Object.keys( value.equipos ).map( index => {
                        return (
                            <li className='list-group-item d-flex justify-content-between align-items-center' key={index}>
                                <p className='m-0'> {value.equipos[index].nombre} </p>
                                <span className='badge btn-danger'>
                                    {value.equipos[index].titulos}
                                </span>
                                <button className='btn btn-success' onClick={ () => {value.sumarTitulo(index)} }> Incrementar </button>
                                <button className='btn btn-danger' onClick={ () => {value.restarTitulo(index)} }> Descrementar </button>
                            </li>
                        );
                    } );
                } }
            </StoreContext.Consumer>
        );
    }
}

export default Equipo;