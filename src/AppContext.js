import React, { Component } from 'react';
import LigaProvider from './components/Context/LigaProvider';
import Equipos from './components/Context/Equipos';

class App extends Component {


  render() {
    return (
      <div className="container">
        <LigaProvider>
            <Equipos />          
        </LigaProvider>
      </div>
    );
  }
}

export default App;
