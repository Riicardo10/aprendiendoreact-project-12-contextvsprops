import React, { Component } from 'react';
import Equipos from './components/Props/Equipos';

class App extends Component {

  state = {
    equipos: [
      {
        nombre:   'Real Madrid',
        titulos:  25
      },
      {
        nombre:   'Barcelona',
        titulos:  19
      },
      {
        nombre:   'Manchester United',
        titulos:  12
      }
    ]
  }

  render() {
    return (
      <div className="container">
        <Equipos 
            equipos={this.state.equipos} />
      </div>
    );
  }
}

export default App;
